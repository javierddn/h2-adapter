package com.javierddn.adapter.h2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class H2AdapterApplication {

	public static void main(String[] args) {
		SpringApplication.run(H2AdapterApplication.class, args);
	}

}
